Read me for converting a test script. 

First off need to give credit to the the book Wicked cool shell scripts by Dave Taylor and Brandon Perry otherwise I would have not been able to do the math for the conversions. 
proper way of running the script should be bash ./[filename] [switch] [temp + unit]
where the switch should only inlcude a lowercase -c (for celcius) -f (fahrenheit) -k (for kelvin) **Must be lowercase and will dictate what the output format will be
the temp + unit should be in the format - numberUPPERCASEUNIT (such as 100K, 100C, or 100F) 
The conversion will take place and output will be based on the switch entered
If no switch is presented output will be all temps 
