#!/usr/bin/bash 

#grab the switch temp number and unit 
temp="$(echo $2|sed -e 's/[^-[:digit:]]*//g')"
unit="$(echo $2|sed -e 's/[-[:digit:]]*//g' | tr '[:lower:]' '[:upper:]' )"
conversion=$1

case ${unit:=F}
	in
	F)
		fahrenheit=$temp #keep the temp if set to F
		celsius="$(echo "scale=2;($temp - 32) / 1.8" | bc)"
		kelvin="$(echo "scale=2;$celsius + 273.15" | bc)" #conversions
		;;
        C)
		celsius=$temp
		kelvin="$(echo "scale=2;$temp + 273.15" | bc)"
		fahrenheit="$(echo "scale=2;(1.8 * $temp) + 32" | bc)"
		;;
	K)
		kelvin=$temp
		celsius="$(echo "scale=2; $temp - 273.15" | bc)" 
		fahrenheit="$(echo "scale=2; (1.8 * $celsius) + 32" | bc)"
		;;

	*)
		echo "Wrong temp, unit or switch given please view the read me"
	
				
esac
# all conversion formula and code w/ the help of Wicked Cool Shell Scripts by dave taylor and brandon perry 

function PrintaTemp { #function with if statement to get the specific switch inputed
	if [ $conversion = "-c" ]
	then
		celsius+="C"
		echo $celsius 
	elif [ $conversion = "-f" ]
	then 
		fahrenheit+="F"
		echo $fahrenheit
	elif [ $conversion = "-k" ]
	then 
		kelvin+="K"
		echo "$kelvin"
	else
		echo "Fahrenheit: $fahrenheit"
		echo "Celsius: $celsius"
		echo "Kelvin: $kelvin"
	fi
}
PrintaTemp $conversion

